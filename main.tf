terraform {
  required_providers {
  }

  backend "http" {
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
    username       = "pat"

    # Set address, lock_address, unlock_address, and password in environment variables
  }
}

